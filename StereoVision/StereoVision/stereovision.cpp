#include "stereovision.h"

StereoVision::StereoVision(){

     pLeftImage = nullptr;
     pRightImage = nullptr;

     pMatrixCameraOne = nullptr;
     pMatrixCameraTwo = nullptr;
     pDistortionCoeffsOne = nullptr;
     pDistortionCoeffsTwo = nullptr;
     pRotationMatrix = nullptr;
     pTranslationVector = nullptr;
     pEssentialMatrix = nullptr;
     pFundamentalMatrix = nullptr;
     pDistortionQMatrix = nullptr;
     pMapXOne = nullptr;
     pMapYOne = nullptr;
     pMapXTwo = nullptr;
     pMapYTwo = nullptr;
     pRotationMatrixOne = nullptr;
     pRotationMatrixTwo = nullptr;
     pTranslationVectorOne = nullptr;
     pTranslationVectorTwo = nullptr;
     pRemapLeftImage = nullptr;
     pRemapRightImage = nullptr;
     pDisparityMap = nullptr;
     pDisparityVisualMap = nullptr;
     BMState = cvCreateStereoBMState();

//     delete[] dataCalibration;
     cvNamedWindow(WindowChessOne,CV_WINDOW_NORMAL);
     cvNamedWindow(WindowChessTwo,CV_WINDOW_NORMAL);
     cvNamedWindow(WindowDisparity,CV_WINDOW_NORMAL);

}

StereoVision::~StereoVision(){

    cvReleaseImage(&pLeftImage);
    cvReleaseImage(&pRightImage);

    cvReleaseMat(&pMatrixCameraOne);
    cvReleaseMat(&pMatrixCameraTwo);
    cvReleaseMat(&pDistortionCoeffsOne);
    cvReleaseMat(&pDistortionCoeffsTwo);
    cvReleaseMat(&pRotationMatrix);
    cvReleaseMat(&pTranslationVector);
    cvReleaseMat(&pEssentialMatrix);
    cvReleaseMat(&pFundamentalMatrix);
    cvReleaseMat(&pDistortionQMatrix);
    cvReleaseMat(&pMapXOne);
    cvReleaseMat(&pMapYOne);
    cvReleaseMat(&pMapXTwo);
    cvReleaseMat(&pMapYTwo);
    cvReleaseMat(&pRotationMatrixOne);
    cvReleaseMat(&pRotationMatrixTwo);
    cvReleaseMat(&pTranslationVectorOne);
    cvReleaseMat(&pTranslationVectorTwo);
    cvReleaseMat(&pRemapLeftImage);
    cvReleaseMat(&pRemapRightImage);
    cvReleaseMat(&pDisparityMap);
    cvReleaseMat(&pDisparityVisualMap);
    cvReleaseStereoBMState(&BMState);
}

void StereoVision::StereoCalibration(){


    int boardWidth = 7;
    int boardHeight = 7;
    int frame = 0;
    int numberBoard = 3;
    int count = 0;
    int resultOne = 0;
    int resultTwo = 0;
    int successImageOne = 0;
    int successImageTwo = 0;
    int countPointOne = 0;
    int countPointTwo = 0;
    int numberPoints = boardWidth*boardHeight;
    int sizePointOne = 0;
    int sizePointTwo = 0;
    float squareSize = 0.026f;

    CvSize imageSize = {0,0};
    CvSize boardSize = cvSize(boardWidth,boardHeight);

    vector<CvPoint2D32f> points[2];
    vector<int> nPoints;
    vector<CvPoint3D32f> tempObjectPoints;
    vector <CvPoint2D32f> tempImagePointOne (numberPoints);
    vector <CvPoint2D32f> tempImagePointTwo (numberPoints);

    double M1[3][3];
    double M2[3][3];
    double D1[5];
    double D2[5];
    double R[3][3];
    double T[3];
    double E[3][3];
    double F[3][3];
    double Q[4][4];

    CvMat tempMatrixCameraOne = cvMat(3,3,CV_64F,M1);
    CvMat tempMatrixCameraTwo = cvMat(3,3,CV_64F,M2);
    CvMat tempDistortionCoeffsOne = cvMat(1,5,CV_64F,D1);
    CvMat tempDistortionCoeffsTwo = cvMat(1,5,CV_64F,D2);
    CvMat tempRotationMatrix = cvMat(3,3,CV_64F,R);
    CvMat tempTranslationVector = cvMat(3,1,CV_64F,T);
    CvMat tempEssentialMatrix = cvMat(3,3,CV_64F,E);
    CvMat tempFundamentalMatrix = cvMat(3,3,CV_64F,F);
    CvMat tempDistortionQMatrix = cvMat(4,4,CV_64F,Q);

    IplImage* pTempFrameOne = nullptr;
    IplImage* pTempFrameTwo = nullptr;
    GetCameraImage(pTempFrameOne,pTempFrameTwo,1);
    IplImage* pGrayFrameOne = cvCreateImage(cvGetSize(pTempFrameOne),8,1);
    imageSize = cvGetSize(pTempFrameOne);


    IplImage* pGrayFrameTwo = cvCreateImage(cvGetSize(pTempFrameTwo),8,1);


    IplImage* pFrameOne = nullptr;
    IplImage* pFrameTwo = nullptr;

    while((successImageOne < numberBoard) || (successImageTwo < numberBoard)){

       GetCameraImage(pFrameOne,pFrameTwo,1);
        ++count;

        if((frame++ % 15) == 0){

            // ------------------------Cam1--------------------------------------------------------------
            resultOne = cvFindChessboardCorners(
                        pFrameOne
                        ,boardSize
                        ,&tempImagePointOne[0]
                        ,&countPointOne
                        ,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
                    );

            cvCvtColor(pFrameOne,pGrayFrameOne,CV_BGR2GRAY);


            //------------------------Cam2---------------------------------------
            resultTwo = cvFindChessboardCorners(
                        pFrameTwo
                        ,boardSize
                        ,&tempImagePointTwo[0]
                        ,&countPointTwo
                        ,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
                    );

            cvCvtColor(pFrameTwo,pGrayFrameTwo,CV_BGR2GRAY);

            if(countPointOne == numberPoints && countPointTwo == numberPoints && resultOne && resultTwo){
                cvFindCornerSubPix(
                            pGrayFrameOne
                            ,&tempImagePointOne[0]
                            ,countPointOne
                            ,cvSize(11,11)
                            ,cvSize(-1,-1)
                            ,cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,30,0.01)
                        );
                cvDrawChessboardCorners(
                            pFrameOne
                            ,boardSize
                            ,&tempImagePointOne[0]
                            ,countPointOne
                            ,resultOne
                        );

                sizePointOne = points[0].size();
                points[0].resize(sizePointOne+numberPoints,cvPoint2D32f(0,0));
                std::copy(tempImagePointOne.begin(),tempImagePointOne.end(), points[0].begin()+sizePointOne);
                successImageOne++;

                cvFindCornerSubPix(
                            pGrayFrameTwo
                            ,&tempImagePointTwo[0]
                            ,countPointTwo
                            ,cvSize(11,11)
                            ,cvSize(-1,-1)
                            ,cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,30,0.01)
                        );
                cvDrawChessboardCorners(
                            pFrameTwo
                            ,boardSize
                            ,&tempImagePointTwo[0]
                            ,countPointTwo
                            ,resultTwo
                        );

                sizePointTwo = points[1].size();
                points[1].resize(sizePointTwo+numberPoints,cvPoint2D32f(0,0));
                std::copy(tempImagePointTwo.begin(),tempImagePointTwo.end(), points[1].begin()+sizePointTwo);
                successImageTwo++;


            }
            cvShowImage(WindowChessOne,pFrameOne);
            cvShowImage(WindowChessTwo,pFrameTwo);
            if(cvWaitKey(15)==27){
                break;
            }
        }
    }

    int totalPoint = numberBoard*numberPoints;
    tempObjectPoints.resize(totalPoint);
    for(int  i = 0; i < boardHeight; ++i){
        for(int j = 0; j < boardWidth; ++j){
            tempObjectPoints[i * boardWidth + j] = cvPoint3D32f((i/ boardHeight)*squareSize,(j % boardHeight)*squareSize,0);
        }
    }

    for(int i = 1; i < numberBoard; ++i){
        std::copy(tempObjectPoints.begin(), tempObjectPoints.begin()+numberPoints,tempObjectPoints.begin() + i*numberPoints);
    }

    nPoints.resize(numberBoard,numberPoints);

    CvMat objectPoints = cvMat(1,totalPoint,CV_32FC3,&tempObjectPoints[0]);
    CvMat imagePointsOne = cvMat(1,totalPoint,CV_32FC2,&points[0][0]);
    CvMat imagePointsTwo = cvMat(1,totalPoint,CV_32FC2,&points[1][0]);
    CvMat NumberOfPoints = cvMat(1,nPoints.size(),CV_32S,&nPoints[0]);


    cvSetIdentity(&tempMatrixCameraOne);
    cvSetIdentity(&tempMatrixCameraTwo);
    cvZero(&tempDistortionCoeffsOne);
    cvZero(&tempDistortionCoeffsTwo);

    cvStereoCalibrate(
                &objectPoints
               ,&imagePointsOne
               ,&imagePointsTwo
               ,&NumberOfPoints
               ,&tempMatrixCameraOne
               ,&tempDistortionCoeffsOne
               ,&tempMatrixCameraTwo
               ,&tempDistortionCoeffsTwo
               ,imageSize
               ,&tempRotationMatrix
               ,&tempTranslationVector
               ,&tempEssentialMatrix
               ,&tempFundamentalMatrix
               ,cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,100,1e-5)
               ,CV_CALIB_FIX_ASPECT_RATIO + CV_CALIB_ZERO_TANGENT_DIST + CV_CALIB_SAME_FOCAL_LENGTH
                );

    cvUndistortPoints(
                &imagePointsOne
               ,&imagePointsOne
               ,&tempMatrixCameraOne
               ,&tempDistortionCoeffsOne
               ,0
               ,&tempMatrixCameraOne
                );

    cvUndistortPoints(
                &imagePointsTwo
               ,&imagePointsTwo
               ,&tempMatrixCameraTwo
               ,&tempDistortionCoeffsTwo
               ,0
               ,&tempMatrixCameraTwo
                );

    pMapXOne = cvCreateMat(imageSize.height,imageSize.width,CV_32F);
    pMapYOne = cvCreateMat(imageSize.height,imageSize.width,CV_32F);
    pMapXTwo = cvCreateMat(imageSize.height,imageSize.width,CV_32F);
    pMapYTwo = cvCreateMat(imageSize.height,imageSize.width,CV_32F);

    double R1[3][3];
    double R2[3][3];
    double P1[3][4];
    double P2[3][4];

    CvMat tempRotationMatrixOne = cvMat(3,3,CV_64F,R1);
    CvMat tempRotationMatrixTwo = cvMat(3,3,CV_64F,R2);

    CvMat tempTranslationVectorOne = cvMat(3,4,CV_64F,P1);
    CvMat tempTranslationVectorTwo = cvMat(3,4,CV_64F,P2);

    cvStereoRectify(
                &tempMatrixCameraOne
               ,&tempMatrixCameraTwo
               ,&tempDistortionCoeffsOne
               ,&tempDistortionCoeffsTwo
               ,imageSize
               ,&tempRotationMatrix
               ,&tempTranslationVector
               ,&tempRotationMatrixOne
               ,&tempRotationMatrixTwo
               ,&tempTranslationVectorOne
               ,&tempTranslationVectorTwo
               ,&tempDistortionQMatrix
               ,0
                );


    cvInitUndistortRectifyMap(
                &tempMatrixCameraOne
               ,&tempDistortionCoeffsOne
               ,&tempRotationMatrixOne
               ,&tempTranslationVectorOne
               ,pMapXOne
               ,pMapYOne
                );


    cvInitUndistortRectifyMap(
                &tempMatrixCameraTwo
               ,&tempDistortionCoeffsTwo
               ,&tempRotationMatrixTwo
               ,&tempTranslationVectorTwo
               ,pMapXTwo
               ,pMapYTwo
                );
    cvSave("Qmatrix.xml",&tempDistortionQMatrix);
    cvSave("MatrixOne.xml",&tempMatrixCameraOne);
    cvSave("MatrixTwo.xml",&tempMatrixCameraTwo);
    cvSave("DistortionMatrixOne.xml",&tempDistortionCoeffsOne);
    cvSave("DistortionMatrixTwo.xml",&tempDistortionCoeffsTwo);
    cvSave("RotationMatrix.xml",&tempRotationMatrix);
    cvSave("RotationMatrixOne.xml",&tempRotationMatrixOne);
    cvSave("RotationMatrixTwo.xml",&tempRotationMatrixTwo);
    cvSave("TranslationVector.xml",&tempTranslationVector);
    cvSave("TranslationVectorOne.xml",&tempTranslationVectorOne);
    cvSave("TranslationVectorOne.xml",&tempTranslationVectorTwo);
    cvSave("MapXOne",pMapXOne);
    cvSave("MapYOne",pMapYOne);
    cvSave("MapXTwo",pMapXTwo);
    cvSave("MapYTwo",pMapYTwo);


}

void StereoVision::GetDisparityMap(){

    while(1){
    assert(BMState != nullptr);

    GetCameraImage(pLeftImage,pRightImage,1);

    pRemapLeftImage = cvCreateMat(pLeftImage->height,pLeftImage->width,CV_8U);
    pRemapRightImage = cvCreateMat(pLeftImage->height,pLeftImage->width,CV_8U);

    pDisparityMap = cvCreateMat(pLeftImage->height,pLeftImage->width,CV_16S);
    pDisparityVisualMap = cvCreateMat(pLeftImage->height,pLeftImage->width,CV_8U);

    cvReleaseImage(&pLeftImage);

    IplImage* tLeftImage = nullptr;
    IplImage* tRightImage = nullptr;
    pLeftImage = cvCreateImage(cvGetSize(pRemapLeftImage),IPL_DEPTH_8U,1);
    pRightImage = cvCreateImage(cvGetSize(pRemapLeftImage),IPL_DEPTH_8U,1);

        GetCameraImage(tLeftImage,tRightImage,1);
        FindContoursImage(tLeftImage);
        DrawDotOnImage(tLeftImage);
        cvCvtColor(tLeftImage,pLeftImage,CV_BGR2GRAY);
        cvCvtColor(tRightImage,pRightImage,CV_BGR2GRAY);

//        cvRemap(pLeftImage,pRemapLeftImage,pMapXOne,pMapYOne);
//        cvRemap(pRightImage,pRemapRightImage,pMapXTwo,pMapYTwo);

        cvFindStereoCorrespondenceBM(pLeftImage,pRightImage,pDisparityMap,BMState);

        cvNormalize(pDisparityMap,pDisparityVisualMap,0,256,CV_MINMAX);

        cvShowImage(WindowDisparity,pDisparityVisualMap);


        CvMat* p3DImage = cvCreateMat(pLeftImage->height,pLeftImage->width,CV_32FC3);
        cvReprojectImageTo3D(pDisparityMap,p3DImage,pDistortionQMatrix);
        cvShowImage(WindowChessOne,p3DImage);
        cvSave("3DImage.xml",p3DImage);
//       cvGet2D(p3DImage,39,179);
        float* ptr = (float*)(p3DImage->data.ptr + 0* p3DImage->step);
         std:: cout << ptr[4*124+2] << std::endl;
//        std::cout < dis << std::endl;
       cvWaitKey(100);
      }
}

void StereoVision::FindContoursImage(IplImage *pImage){

    assert(pImage != nullptr);

    IplImage* pGrayImage = cvCreateImage(cvGetSize(pImage),IPL_DEPTH_8U,1);
    IplImage* pImageContours = cvCreateImage(cvGetSize(pImage),IPL_DEPTH_8U,1);
    pContoursStorage = cvCreateMemStorage(0);

    cvCvtColor(pImage,pGrayImage,CV_BGR2GRAY);
    cvCanny(pGrayImage,pImageContours,100,200,3);

    cvFindContours(
            pImageContours
            ,pContoursStorage
            ,&pHierarchyContours
            ,sizeof(CvContour)
            ,CV_RETR_EXTERNAL
            ,CV_CHAIN_APPROX_NONE
            ,cvPoint(0,0)
                );

    cvReleaseImage(&pGrayImage);
    cvReleaseImage(&pImageContours);
}

void StereoVision::DrawDotOnImage(IplImage* pImage){
    assert(pImage != nullptr);

    for(CvSeq* seq = pHierarchyContours; seq != nullptr; seq = seq->h_next){
    CvPoint* point = (CvPoint*)cvGetSeqElem(seq,seq->first->count);
    cvCircle(
            pImage
            ,cvPoint(point->y,point->x)
            ,5
            ,cvScalarAll(255)
            );
    std::string str = std::to_string(point->y) + " " + std::to_string(point->x);
    CvFont myFont;
    cvInitFont( &myFont, CV_FONT_HERSHEY_COMPLEX,0.5, 0.5, 0, 1, CV_AA);
    cvPutText(pImage,str.c_str(),cvPoint(point->y,point->x),&myFont,cvScalar(0,255,255));
    }

//    cvShowImage("imageContour",pImage);
}

double StereoVision::CalculateDistance(CvMat *&disparityMap, int row, int coll){

        double focal = dataCalibration[0];
        double baseLine = 9;
        double 	k1  = 8e-7,
                k2  = -0.001,
                k3  = 0.4,
                k4  = 17.44;
        double 	depth = 0;
        CvMat* tempMap = cvCreateMat(disparityMap->height, disparityMap->width,CV_32F);
        IplImage* real_disparity= cvCreateImage( cvSize(disparityMap->height,disparityMap->width), IPL_DEPTH_32F, 1 );
            cvConvertScale( disparityMap, real_disparity, 1.0/16, 0 );
        for(int y = 0; y < disparityMap->height; ++y){

            for(int x = 0; x < disparityMap->width; ++x){

                if(cvGet2D(pDisparityVisualMap,y,x).val[0] > 0){

                    depth = (baseLine*focal)/(cvGet2D(disparityMap,y,x).val[0]/(-16));
                    depth = (k1*depth*depth*depth+k2*depth*depth+k3*depth+k4);

                }else{
                    depth = 0;
                }
                cvmSet(tempMap,y,x,depth);
            }
        }


        FILE *distanceFile;
            stringstream strDistance;
            strDistance<<"Distance.txt";
            string Distance = strDistance.str();
            distanceFile = fopen(&Distance[0],"w");

            for (int y = 0; y < pDisparityVisualMap->rows; y++){
                for (int x = 0; x < pDisparityVisualMap->cols ; x++){

                        fprintf(distanceFile, "%d %d %f %f\n",y,x,(float)cvmGet(tempMap,y,x),(float)cvGet2D(real_disparity,y,x).val[0]);
                    }
            }
        fclose(distanceFile);
        }

void StereoVision::StereoVisionStart(){
    //camera->InitBoardPhotography(40);
    this->StereoCalibration();
    this->LoadDisparityParam();
    this->LoadCalibrationData();
    this->GetDisparityMap();
    this->CalculateDistance(pDisparityMap,45,45);


}

void StereoVision::GetCameraImage(IplImage *&imageOne, IplImage *&imageTwo, int channel){

    CvCapture* pCameraOne = cvCreateCameraCapture(1);
    for(int i = 0; i < 2; ++i){
        imageOne = cvQueryFrame(pCameraOne);

    }
    cvSaveImage("LeftImage.jpg",imageOne);

    cvReleaseCapture(&pCameraOne);

    pCameraOne = cvCreateCameraCapture(2);
    for(int i = 0; i < 2; ++i){

        imageTwo = cvQueryFrame(pCameraOne);

    }
     cvSaveImage("RightImage.jpg",imageTwo);

    cvReleaseCapture(&pCameraOne);

    imageOne = cvLoadImage("LeftImage.jpg",channel);
    imageTwo = cvLoadImage("RightImage.jpg",channel);


}

void StereoVision::LoadCalibrationData(){


    pDistortionQMatrix = (CvMat*)cvLoad("Qmatrix.xml");
    pMatrixCameraOne = (CvMat*)cvLoad("MatrixOne.xml");
    pMatrixCameraTwo = (CvMat*)cvLoad("MatrixTwo.xml");
    pDistortionCoeffsOne = (CvMat*)cvLoad("DistortionMatrixOne.xml");
    pDistortionCoeffsTwo = (CvMat*)cvLoad("DistortionMatrixTwo.xml");
    pRotationMatrix = (CvMat*)cvLoad("RotationMatrix.xml");
    pRotationMatrixOne = (CvMat*)cvLoad("RotationMatrixOne.xml");
    pRotationMatrixTwo = (CvMat*)cvLoad("RotationMatrixTwo.xml");
    pTranslationVector = (CvMat*)cvLoad("TranslationVector.xml");
    pTranslationVectorOne = (CvMat*)cvLoad("TranslationVectorOne.xml");
    pTranslationVectorTwo = (CvMat*)cvLoad("TranslationVectorTwo.xml");
    pMapXOne = (CvMat*)cvLoad("MapXOne");
    pMapYOne = (CvMat*)cvLoad("MapYOne");
    pMapXTwo = (CvMat*)cvLoad("MapXTwo");
    pMapYTwo = (CvMat*)cvLoad("MapYTwo");

    dataCalibration = new double[6];
    dataCalibration[0] = cvmGet(pMatrixCameraOne,0,0); // focal lenght x
    dataCalibration[1] = cvmGet(pMatrixCameraOne,0,0); // focal lenght y
    dataCalibration[2] = (-1)*cvmGet(pTranslationVector,0,0);

}

void StereoVision::LoadDisparityParam(){

    int numberDispatirityParam = 10;

    ifstream fileDisparityData;
    fileDisparityData.open("DisparityData.txt");
    if(!fileDisparityData.is_open()){
        std::cerr << "File is not open. Perhaps file is not found." << endl;
        exit(0);
    }else{
        string nameParam;
        int valueParam  = 0;

      for(int i = 0; i < numberDispatirityParam; ++i ){
          fileDisparityData >> nameParam;
          fileDisparityData >> valueParam;
          disparityData[i] = valueParam;
      }
    }

    fileDisparityData.close();
}


