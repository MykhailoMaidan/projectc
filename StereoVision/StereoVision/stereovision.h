#ifndef STEREOVISION_H
#define STEREOVISION_H

//opencv
#include <opencv/cv.h>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

//c++
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include "webcameramanager.h"

using namespace std;

#define WindowChessOne "WindowChessOne"
#define WindowChessTwo "WindowChessTwo"
#define WindowDisparity "WindowDispatirity"



class StereoVision{

public:
    //member
    CvStereoBMState* BMState;
     //func
    StereoVision();

    ~StereoVision();

    void CalibrationChambers();

    void StereoCalibration();

    void GetDisparityMap();

    void FindContoursImage(IplImage *pImage);

    void DrawDotOnImage(IplImage* pImage);

    double CalculateDistance(CvMat*& dispatirityMap, int row, int coll);

    void StereoVisionStart();

    void GetCameraImage(IplImage*& imageOne, IplImage*& imageTwo, int channel);

private:
    WebCameraManager* camera;

    IplImage* pLeftImage;
    IplImage* pRightImage;

    CvMat* pMatrixCameraOne;
    CvMat* pMatrixCameraTwo;
    CvMat* pDistortionCoeffsOne;
    CvMat* pDistortionCoeffsTwo;
    CvMat* pRotationMatrix;
    CvMat* pTranslationVector;
    CvMat* pEssentialMatrix;
    CvMat* pFundamentalMatrix;
    CvMat* pDistortionQMatrix;
    CvMat* pMapXOne;
    CvMat* pMapYOne;
    CvMat* pMapXTwo;
    CvMat* pMapYTwo;
    CvMat* pRotationMatrixOne;
    CvMat* pRotationMatrixTwo;
    CvMat* pTranslationVectorOne;
    CvMat* pTranslationVectorTwo;
    CvMat* pRemapLeftImage;
    CvMat* pRemapRightImage;
    CvMat* pDisparityMap;
    CvMat* pDisparityVisualMap;
    CvMemStorage* pContoursStorage;
    CvSeq* pHierarchyContours;


    double* dataCalibration;
    int disparityData[10];

    //-------features-------
    void LoadCalibrationData();

    void LoadDisparityParam();


};

#endif // STEREOVISION_H
