#ifndef CAMERACAPTURE_H
#define CAMERACAPTURE_H

#include <opencv/cv.h>
#include <opencv/highgui.h>


struct CameraCapture{

public:

    CameraCapture();

    ~CameraCapture();

    void InitCamera();

    IplImage* LeftImage(int channel);

    IplImage* RightImage(int channel);

private:
    CvCapture* pCameraLeft;
    CvCapture* pCameraRight;
    IplImage* pLeftImage;
    IplImage* pRightImage;
};

#endif // CAMERACAPTURE_H
