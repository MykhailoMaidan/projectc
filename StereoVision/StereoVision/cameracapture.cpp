#include "cameracapture.h"

CameraCapture::CameraCapture(){
    pCameraLeft = nullptr;
    pCameraRight = nullptr;
    pLeftImage = nullptr;
    pRightImage = nullptr;

}

CameraCapture::~CameraCapture(){
    cvReleaseCapture(&pCameraLeft);
    cvReleaseCapture(&pCameraRight);
    cvReleaseImage(&pLeftImage);
    cvReleaseImage(&pRightImage);
}

void  CameraCapture::InitCamera(){
    pCameraLeft = cvCreateCameraCapture(0);
    pLeftImage = cvQueryFrame(pCameraLeft);
    cvSaveImage("LeftImage.jpg",pLeftImage);
    cvReleaseCapture(&pCameraLeft);


    pCameraRight = cvCreateCameraCapture(1);
    pRightImage = cvQueryFrame(pCameraRight);
    cvSaveImage("RightImageewdsdfxv.jpg",pRightImage);
    cvReleaseCapture(&pCameraRight);
}

IplImage *CameraCapture::LeftImage(int channel){
    pLeftImage = cvLoadImage("LeftImage.jpg",channel);
    return pLeftImage;
}

IplImage *CameraCapture::RightImage(int channel){
    pRightImage = cvLoadImage("RightImage.jpg",channel);
    return pRightImage;

}


