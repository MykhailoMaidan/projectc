#include <iostream>
#include "stereovision.h"
#include "webcameramanager.h"
using namespace std;


const char* SADWindowSize = "SADWindowSize";
const char* TextureThreshold = "TextureThreshold";
const char* UniquenessRatio = "UniquenessRatio";
const char* NumberOfDisparities = "NumberOfDisparities";
const char* PreFilterSize = "PreFilterSize";
const char* PreFilterCap = "PreFilterCap";
const char* SpeckWindowSize = "SpeckWindowSize";
const char* MinDisparity = "MinDisparity";
const char* SpeckleRange = "SpeckleRange";
const char* Disp12MaxDiff = "Disp12MaxDiff";

StereoVision* obj = new StereoVision();
WebCameraManager* camera = new WebCameraManager();


void CreateTrackBarAll();

int main(int argc, char *argv[])
{
//    std::vector<int> index;
//    index.push_back(0);
//    index.push_back(2);
//    camera->StartParalleCaptureWebCameras(2,index);

    CreateTrackBarAll();
    obj->StereoVisionStart();



}

void TrackbarSADWindowSize(int pos){
   obj->BMState->SADWindowSize = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//   obj->GetDisparityMap();

}

void TrackbarTextureThreshold(int pos){
   obj->BMState->textureThreshold = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//   obj->GetDisparityMap();
}

void TrackbarUniquenessRatio(int pos){
    obj->BMState->uniquenessRatio = pos;
//    obj->GetDisparityMap();
}

void TrackbarNumberOfDisparities(int pos){
    obj->BMState->numberOfDisparities = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//    obj->GetDisparityMap();
}

void TrackbarPreFilterSize(int pos){
    obj->BMState->preFilterSize = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//    obj->GetDisparityMap();
}

void TrackbarPreFilterCap(int pos){
    obj->BMState->preFilterCap = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//    obj->GetDisparityMap();
}

void TrackbarSpeckWindowSize(int pos){
    obj->BMState->speckleWindowSize = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
    /*obj->GetDisparityMap()*/;
}

void TrackbarMinDisparity(int pos){
    obj->BMState->minDisparity = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//    obj->GetDisparityMap();
}

void TrackbarSpeckleRange(int pos){
    obj->BMState->speckleRange = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//    obj->GetDisparityMap();
}

void TrackbarDisp12MaxDiff(int pos){
    obj->BMState->disp12MaxDiff = cvGetTrackbarPos(SADWindowSize,WindowDisparity);
//    obj->GetDisparityMap();
}

void CreateTrackBarAll(){
    int currentPosition = 5;
    int maxValue = 255;
    cvCreateTrackbar(SADWindowSize,WindowDisparity,&currentPosition,maxValue,TrackbarSADWindowSize);
    cvCreateTrackbar(TextureThreshold,WindowDisparity,&currentPosition,maxValue,TrackbarTextureThreshold);
    cvCreateTrackbar(UniquenessRatio,WindowDisparity,&currentPosition,maxValue,TrackbarUniquenessRatio);
    cvCreateTrackbar(NumberOfDisparities,WindowDisparity,&currentPosition,maxValue,TrackbarNumberOfDisparities);
    cvCreateTrackbar(PreFilterSize,WindowDisparity,&currentPosition,maxValue,TrackbarPreFilterSize);
    cvCreateTrackbar(PreFilterCap,WindowDisparity,&currentPosition,maxValue,TrackbarPreFilterCap);
    cvCreateTrackbar(SpeckWindowSize,WindowDisparity,&currentPosition,maxValue,TrackbarSpeckWindowSize);
    cvCreateTrackbar(MinDisparity,WindowDisparity,&currentPosition,maxValue,TrackbarMinDisparity);
    cvCreateTrackbar(SpeckleRange,WindowDisparity,&currentPosition,maxValue,TrackbarSpeckleRange);
    cvCreateTrackbar(Disp12MaxDiff,WindowDisparity,&currentPosition,maxValue,TrackbarDisp12MaxDiff);
}


