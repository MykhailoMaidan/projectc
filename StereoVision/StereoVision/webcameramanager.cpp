#include "webcameramanager.h"

std::string WebCameraManager::sPathToImage = "../BoardImageSrc/";
const std::string WebCameraManager::nameLeftImage = "LeftImage";
const std::string WebCameraManager::nameRightImage = "RightImage";

WebCameraManager::WebCameraManager(){

    thread = nullptr;

}

WebCameraManager::~WebCameraManager(){

    delete thread;
}

bool WebCameraManager::StartParalleCaptureWebCameras(int numberOfWebCameras, std::vector<int> &indexWebCameras){

    int resultCreateThread = 0;
    if(numberOfWebCameras != indexWebCameras.size() ){
        std::cerr << "Number of web cameras is not equal number index of web cameras!" << std::endl;
    }

    try{
    thread = new pthread_t[numberOfWebCameras];
    }catch(std::bad_alloc e){
     std::cerr << "Bad alloc" << std::endl;
     return false;
    }

    for(int i = 0; i < numberOfWebCameras; ++i){

      resultCreateThread = pthread_create(&thread[i],NULL,Capture,static_cast<void*>(&indexWebCameras[i]));

      if(resultCreateThread != 0){

          std::cerr << "Create " << i << "thread is fail." << std::endl;
          return false;
      }
    }

    for(int i = 0; i < numberOfWebCameras; ++i){
        pthread_join(thread[i],nullptr);
    }

    return true;
}

bool WebCameraManager::InitBoardPhotography(int numberOfPhotography){

    CvCapture* camera =  nullptr;
    IplImage* pFrame = nullptr;
    cvNamedWindow("w1",CV_WINDOW_NORMAL);
    cvNamedWindow("w2",CV_WINDOW_NORMAL);
    for(int i  = 0; i < numberOfPhotography; ++i){

        camera = cvCreateCameraCapture(0);
        static std::string nameImage;
        nameImage = sPathToImage +  nameLeftImage;
        pFrame = cvQueryFrame(camera);
        cvShowImage("w1",pFrame);
        nameImage += std::to_string(i) + ".jpg";
        cvSaveImage(nameImage.c_str(),pFrame);
        nameImage.clear();
        cvReleaseImage(&pFrame);
        cvReleaseCapture(&camera);

        camera = cvCreateCameraCapture(2);
        nameImage = sPathToImage +  nameRightImage;
        pFrame = cvQueryFrame(camera);
        cvShowImage("w2",pFrame);
        nameImage += std::to_string(i) + ".jpg";
        cvSaveImage(nameImage.c_str(),pFrame);
        nameImage.clear();
        cvReleaseImage(&pFrame);
        cvReleaseCapture(&camera);

        if(cvWaitKey(15)==33){
            break;
        }

    }


}

bool WebCameraManager::LoadBoardImage(IplImage *& pLeftImage, IplImage *& pRightImage, int channel, int indexPhoto){

    std::string leftImage = sPathToImage + nameLeftImage + std::to_string(indexPhoto) + ".jpg";
    std::string rightImage = sPathToImage + nameRightImage + std::to_string(indexPhoto) +".jpg";
    pLeftImage = cvLoadImage(leftImage.c_str(),channel);
    pRightImage = cvLoadImage(rightImage.c_str(),channel);

}

void *WebCameraManager::Capture(void *arg){

    int indexWebCamera = *(static_cast<int*>(arg));

    CvCapture* pCamera = cvCreateCameraCapture(indexWebCamera);
    IplImage* pFrame = cvQueryFrame(pCamera);
    std::string nameImage = "Image" + std::to_string(indexWebCamera) + ".jpg";
    std::string nameWindow = "Window" + std::to_string(indexWebCamera);
    cvNamedWindow(nameWindow.c_str(),CV_WINDOW_NORMAL);
    while(cvWaitKey(15) != 33){
        pFrame = cvQueryFrame(pCamera);
        cvSaveImage(nameImage.c_str(),pFrame);
        cvShowImage(nameWindow.c_str(),pFrame);



    }

    cvReleaseCapture(&pCamera);
    cvReleaseImage(&pFrame);

}
