#ifndef EMULATORBILLVALIDATOR_H
#define EMULATORBILLVALIDATOR_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <cstdint>
#include <cstdlib>

enum CommandLeng{
    ASK = 6,
    INITIALIZING = 6,
    STATUS = 11,
    IDENTIFICATION = 39,
    BILL_TABLE = 125,
    INVALID = 6,
    IDLING = 6,
    DISABLED = 6,
    BILL = 7
};

struct CommandsRequest{
    uint8_t ASK [6] = {2,3,6,0,194,130};
    uint8_t RESET [6] = {2,3,6,48,65,179};               // 02 03 06 30 41 B3
    uint8_t POLL [6] = {2,3,6,51,218,129};               // 02 03 06 33 DA 81
    uint8_t STATUS_REQUEST [6] = {2,3,6,49,200,162};     // 02 03 06 31 C8 A2
    uint8_t IDENTIFICATION [6] = {2,3,6,55,254,199};     // 02 03 06 37 FE C7
    uint8_t BILL_TABLE [6] = {2,3,6,65,79,209};          // 02 03 06 41 4F D1
    uint8_t ENABLEBILL [12] = {2,3,12,52,255,255,        // 02 03 0C 34 FF FF
                             255,255,255,0,181,191};     // FF FF FF 00 C2 82
    uint8_t DISABLEBILL [12] = {2,3,6,52,0,0,            // 02 03 06 34 00 00
                                0,0,0,0,35,18}           // 00 00 00 00 23 12 
};

struct CommandSend{
    uint8_t ASK[6] = {2,3,6,0,194,130};                  // 02 03 06 00 C2 82
    uint8_t INITIALIZING[6] = {2,3,6,19,216,160};        // 02 03 06 13 D8 A0
    uint8_t IDLING [6] = {2,3,6,20,103,212};             // 02 03 06 14 67 D4
    uint8_t INVALID [6] = {2,3,6,48,65,179};             // 02 03 06 30 41 B3
    uint8_t DISABLED [6] = {2,3,6,25,130,15};            // 02 03 06 19 82 0F    
    uint8_t STATUS[11] = {2,3,11,0,0,0,0,                // 02 03 0B 00 00 00
                          0,0,168,103};                  // 00 00 00 A8 67 7

    uint8_t IDENTIFICATION_SEND[39] = {2,3,39,83,77,45,  // 02 03 27 53 4D 2D
                                     85,83,49,55,51,53,  // 55 53 31 37 33 35
                                     32,32,32,32,32,32,  // 20 20 20 20 20 20
                                     50,48,75,67,50,55,  // 32 30 4B 43 32 37
                                     48,48,53,56,55,51,  // 30 30 35 38 37 33
                                     87,0,77,83,38,18,   // 57 00 4D 53 26 12
                                     240,18,1};          // F0,12,01

    uint8_t BILL_TABLE_SEND [125] = {2,3,125,1,85,83,    // 02 03 7D 01 55 53
                                  65,0,2,85,83,65,       // 41 00 02 55 53 41
                                  0,5,85,83,65,0,        // 00 05 55 53 41 00
                                  1,85,83,65,1,2,        // 01 55 53 41 01 02
                                  85,83,65,1,5,85,       // 55 53 41 01 05 85
                                  83,65,1,1,85,83,       // 53 41 01 01 55 53
                                  65,2,0,0,0,0,          // 41 02 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00 
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,0,0,           // 00 00 00 00 00 00
                                  0,0,0,0,1,66,          // 00 00 00 00 01 42 
                                  65,82,0,134,22};       // 41 52 00 86 16
};

struct BillNominal{
    uint8_t ONEDOLLAR [7] = {2,3,7,129,0,84,42};         // 02 03 07 81 00 54 42
    uint8_t FIVEDOLLAR [7] = {2,3,7,129,2,70,9};         // 02 03 07 81 02 46 09
    uint8_t TENDOLLAR [7] = {2,3,7,129,3,207,24};        // 02 03 07 81 03 CF 18
    uint8_t TWENTYDOLLAR [7] = {2,3,7,129,4,112,108};    // 02 03 07 81 04 70 6C
    uint8_t FIFTYDOLLAR [7] = {2,3,7,129,5,249,125};     // 02 03 07 81 05 F9 7D
};
          

class EmulatorBillValidator {

public:
    EmulatorBillValidator();
    ~EmulatorBillValidator();
    bool OpenSerialPort(char * namePort);
    void ConfigureSerialPort();
    void Start();

private:
    static const uint8_t LENGTH_OF_RECEIVED_DATA = 12;
    int randomNumber;
    bool mIdentificationBillValidatorFlag;
    uint8_t mCounterPoll;
    uint8_t mStatePoll;
    uint8_t mDescriptorPort;
    struct termios mSerialPortSettings;
    uint8_t mRequest[6];
    
    CommandsRequest * mCommandRequest;
    CommandSend * mCommandToSend;
    BillNominal * mDollarsNominal;
    CommandLeng  mLenghtAnswer;
    
    
    uint8_t * ReadFromSerialPort();
    bool WriteToSerialPort(uint8_t* sendCommand,CommandLeng & lenghtAnswer);
    uint8_t * CompareCommand(uint8_t * array, CommandLeng & lenghtAnswer);
    uint8_t * GetRandomBill();
    uint8_t * RandomIndingOrBillAnswer(CommandLeng & lenghtAnswer);


};


#endif // EMULATORBILLVALIDATOR_H
