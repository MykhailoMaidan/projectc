#ifndef HELLP_H
#define HELLP_H


#include "MFRC522.h"


class Hellp{

private:

public:
	Hellp();
	bool try_key(MFRC522::MIFARE_Key *key);
	void dump_byte_array(byte *buffer, byte bufferSize);
};

#endif
