/* The classic "blink" example
 *
 * This sample code is in the public domain.
 */
#include <stdlib.h>

 //esp-opne-rtos lib
#include "espressif/esp_common.h"
#include "FreeRTOS.h"
#include "esp8266.h"
#include "task.h"
#include "queue.h"

#include "esp/uart.h"

//RFID lib
#include "MFRC522.h"
#include "Hellp.h"

typedef uint8_t byte;

const byte RESET_PIN = 4;
const byte SLAVE_SELECT_PIN = 15;
const byte NR_KNOWN_KEYS = 8;

MFRC522 mfrc522(SLAVE_SELECT_PIN,RESET_PIN);
Hellp temp;


/*byte knownKeys[NR_KNOWN_KEYS][MFRC522::MF_KEY_SIZE] =  {
    {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}, // FF FF FF FF FF FF = factory default
    {0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5}, // A0 A1 A2 A3 A4 A5
    {0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5}, // B0 B1 B2 B3 B4 B5
    {0x4d, 0x3a, 0x99, 0xc3, 0x51, 0xdd}, // 4D 3A 99 C3 51 DD
    {0x1a, 0x98, 0x2c, 0x7e, 0x45, 0x9a}, // 1A 98 2C 7E 45 9A
    {0xd3, 0xf7, 0xd3, 0xf7, 0xd3, 0xf7}, // D3 F7 D3 F7 D3 F7
    {0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff}, // AA BB CC DD EE FF
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}  // 00 00 00 00 00 00
};*/


void task(void* pvParameters)
    {
   while(1){
   
        if ( ! mfrc522.PICC_IsNewCardPresent()){
            break;
        }

           // vTaskDelay(100 / portTICK_PERIOD_MS);
        

    /*// Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;

    // Show some details of the PICC (that is: the tag/card)
    printf("Card UID:");
    temp.dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    printf("\n");
    printf("PICC type: ");
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    //printf(mfrc522.PICC_GetTypeName(piccType));
    
    // Try the known default keys
    MFRC522::MIFARE_Key key;
    for (byte k = 0; k < NR_KNOWN_KEYS; k++) {
        // Copy the known key into the MIFARE_Key structure
        for (byte i = 0; i < MFRC522::MF_KEY_SIZE; i++) {
            key.keyByte[i] = knownKeys[k][i];
        }
        // Try the key
        if (temp.try_key(mfrc522,&key)) {
            // Found and reported on the key and block,
            // no need to try other keys for this PICC
            break;
        }
    }*/
    }
}


extern "C" void user_init(void)
{
    uart_set_baud(0, 9600);
    printf("SDK version:%s\n\n", sdk_system_get_sdk_version());

    spi_init(1, SPI_MODE0, SPI_FREQ_DIV_1M, 1, SPI_LITTLE_ENDIAN, false); // init SPI module
                                                      // Init MFRC522 card
    printf("Inithialization SPI and MFRC522");
    mfrc522.PCD_Init();
  
    
    xTaskCreate(task,"task",8192,NULL,2,NULL);
    //xTaskCreate(blinkenRegisterTask, "blinkenRegisterTask", 256, NULL, 2, NULL);
}