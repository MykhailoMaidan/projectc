#include "rfid_rc522.h"

void RFIDInit(){

  gpio_enable(15,GPIO_OUTPUT);
  gpio_write(15,1);

  
  gpio_enable(4,GPIO_OUTPUT);
  gpio_write(4,1);

    //reset  baud  rates
  RFIDWriteRegisterTwo(TxModeReg,0x00);
  RFIDWriteRegisterTwo(RxModeReg,0x00);

    //reset ModWidhtReg
  RFIDWriteRegisterTwo(ModWidthReg,0x26);

  RFIDWriteRegisterTwo(TModeReg,0x80);
  RFIDWriteRegisterTwo(TPrescalerReg,0xA9);
  RFIDWriteRegisterTwo(TReloadRegH,0x03);
  RFIDWriteRegisterTwo(TReloadRegL,0xE8);

  RFIDWriteRegisterTwo(TxASKReg,0x40);
  RFIDWriteRegisterTwo(ModeReg,0x3D);
  RFIDAntenaOn();

}


      

void RFIDWriteRegisterTwo(Registers reg, uint8_t value){
	spi_init(1,SPI_MODE0,SPI_FREQ_DIV_1M, 1, SPI_LITTLE_ENDIAN, true);
	gpio_write(15,0);

	//send data to rfid
	spi_transfer(1, &reg,0,sizeof(reg), SPI_8BIT);
	spi_transfer(1, &value,0,sizeof(reg), SPI_8BIT);

  gpio_write(15,1);

}
/**
 * Writes a number of bytes to the specified register in the MFRC522 chip.
 * The interface is described in the datasheet section 8.1.2.
 */
void RFIDWriteRegisterThree(Registers reg, const uint8_t size, const uint8_t * value){

  gpio_write(15,0);
  spi_transfer(1,&reg,0,sizeof(reg),SPI_8BIT);
  
  for(uint8_t i = 0; i < size; ++i){

    spi_transfer(1,value[i],0,sizeof(value[i]),SPI_8BIT);
  }

  gpio_write(15,1);

}

uint8_t RFIDReadRegister(Registers reg){
  uint8_t value;
  gpio_write(15,0);

  uint8_t temp = 1 | reg;
  spi_transfer(1,&temp,0,sizeof(temp),SPI_8BIT);
  spi_transfer(1,0,&value,sizeof(value),SPI_8BIT);

  gpio_write(15,1);

  return value;

}

/**
 * Reads a number of bytes from the specified register in the MFRC522 chip.
 * The interface is described in the datasheet section 8.1.2.
*/
void RFIDReadRegisterFour(Registers reg, uint8_t count, uint8_t * values, uint8_t rxAlign){

  if(count == 0){
    return;
  }

  uint8_t address = 0x80 | reg; // MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
  uint8_t index = 0;

  gpio_write(15,0);
  --count;

  spi_transfer(1,&address,0,sizeof(address),SPI_8BIT);

  if(rxAlign){            // Only update bit positions rxAlign..7 in values[0]
    // Create bit mask for bit positions rxAlign..7
    uint8_t mask = (0xFF << rxAlign) & 0xFF;

    // Read value and tell that we want to read the same address again.
    uint8_t value;
    spi_transfer(1,&address,&value,sizeof(value),SPI_8BIT);

    values[0] = (values[0] & ~ mask) | (value & mask);
    ++index;

  }

  while(index < count){
    spi_transfer(1,address,values[index],sizeof(values[index]),SPI_8BIT);  // Read value and tell that we want to read the same address again.
    ++index;
  }

  spi_transfer(1,0,values[index],sizeof(values[index]),SPI_8BIT);
  gpio_write(15,1);
}

/*
Turns the antenna on by enabling pins TX1 and TX2.
 * After a reset these pins are disabled.
*/
void RFIDAntenaOn(){
  uint8_t value  = RFIDReadRegister(TxControlReg);

  if((value & 0x03) != 0x03){
    
    RFIDWriteRegisterTwo(TxControlReg,value | 0x03); 
  }
}

/**
 * Returns true if a PICC responds to PICC_CMD_REQA.
 * Only "new" cards in state IDLE are invited. Sleeping cards in state HALT are ignored.
 * 
 * @return bool
 */

bool RFIDIsNewCardPresent(){

  uint8_t bufferData[2];
  uint8_t sizeBuff = sizeof(bufferData);

  //reset bade rate
  RFIDWriteRegisterTwo(TxModeReg,0x00);
  RFIDWriteRegisterTwo(RxModeReg,0x00);

  //reset ModWidthReg
  RFIDWriteRegisterTwo(ModWidthReg,0x26);
  return 0;

}

/**
 * Transmits a REQuest command, Type A. Invites PICCs in state IDLE to go to READY and prepare for anticollision or selection. 7 bit frame.
 * Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT - probably due do bad antenna design.
 * 
 * @return STATUS_OK on success, STATUS_??? otherwise.
 */

StatusCode RFIDRequestA(uint8_t * bufferData,  const uint8_t  *bufferSize){

}
/**
 * Clears the bits given in mask from register reg.
 */

void RFIDClearRegisterBitMAsk(Registers reg, uint8_t mask){

  uint8_t temp;
  temp = RFIDReadRegister(reg);
  RFIDWriteRegisterTwo(reg,temp & (~mask));

}

/**
 * Sets the bits given in mask in register reg.
 */

void RFIDSetRegisterBitMask(Registers reg, uint8_t mask){
  uint8_t temp;
  temp = RFIDReadRegister(reg);
  RFIDWriteRegisterTwo(reg,temp | mask);

}


/**
 * Transmits REQA or WUPA commands.
 * Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT - probably due do bad antenna design.
 * 
 * @return STATUS_OK on success, STATUS_??? otherwise.
 */ 

StatusCode RFIDRequest(const uint8_t * command, uint8_t * bufferData, uint8_t * bufferSize){

  uint8_t validBits;
  StatusCode status;

  if(bufferData == NULL || *bufferSize < 2){
    return STATUS_NO_ROOM;
  }

  RFIDClearRegisterBitMAsk(CollReg,0x80);
  validBits = 7;

 return STATUS_OK;
}

StatusCode CommunicationWithRFID(const  uint8_t command, const uint8_t waitIRQ, const uint8_t * inputSendData,
                 const uint8_t * inputSendLen, uint8_t * outputBackData, uint8_t * outputBackLen,
                 uint8_t * inputValidBits, uint8_t rxAlign, bool checkCRC
                 ){

  //prepare values for BitFrammingReg
  uint8_t txLastBits = inputValidBits ? *inputValidBits : 0;
  uint8_t bitFraming = (rxAlign << 4) + txLastBits;

  RFIDWriteRegisterTwo(CommandReg,PCD_Idle);                       // Stop any active command.
  RFIDWriteRegisterTwo(ComIrqReg,0x7F);                            // Clear all seven interrupt request bits
  RFIDWriteRegisterTwo(FIFOLevelReg,0x80);                         // FlushBuffer = 1, FIFO initialization
  RFIDWriteRegisterThree(FIFODataReg, inputSendLen, inputSendData);  // Write sendData to the FIFO
  RFIDWriteRegisterTwo(BitFramingReg,bitFraming);                // Bit adjustments
  RFIDWriteRegisterTwo(CommandReg,command);                        // Execute the command
  
  if(command == PCD_Transceive){
      RFIDSetRegisterBitMask(BitFramingReg,0x80);              // StartSend=1, transmission of data starts
  }

  uint8_t i = 0;
  uint8_t number = ;
  for(i = 2000; i > 0; --i){
     number = RFIDReadRegister(ComIrqReg); //ComIrqReg[7..0] bits are: Set1 TxIRq RxIRq IdleIRq HiAlertIRq LoAlertIRq ErrIRq TimerIRq

      if(number & waitIRQ){                         // One of the interrupts that signal success has been set.sa
        break;
      }
      // 35.7ms and nothing happend. Communication with the MFRC522 might be down
      if(n & 0x01){

        return STATUS_TIMEOUT;
      }
  }

  if(i == 0){
    return STATUS_TIMEOUT;
  }

  // Stop now if any errors except collisions were detected.
  uint8_t errorRegValue = RFIDReadRegister(ErrorReg);
  if(errorRegValue & 0x13){  // ErrorReg[7..0] bits are: WrErr TempErr reserved BufferOvfl CollErr CRCErr ParityErr ProtocolErr
    return STATUS_ERROR;
  }
  

  uint8_t _validBits = 0;

  // If the caller wants data back, get it from the MFRC522.
  if(outputBackData && outputBackLen){
    uint8_t temp = RFIDReadRegister(FIFOLevelReg); // number of bytes in the FIFO

    if(temp > *outputBackLen){
      return STATUS_NO_ROOM;
    }

    *outputBackLen = temp;
    RFIDReadRegisterFour(FIFOLevelReg,temp,outputBackData,rxAlign); //get recieve data from FIFO

    _validBits RFIDReadRegister(CommandReg) & 0x07; // RXLastBits [2:0]   indicates the number of

    if(validBits){
      *validBits = _validBits;
    }

  }


  //Tell about collisions

  if(errorRegValue & 0x08){

    return STATUS_COLLISION;
  }

  //Perform CRC_A validation if requested

  if(outputBackData && outputBackLen && checkCRC){

    if(*outputBackLen == 1 && _validBits == 4){

      return STATUS_MIFARE_NACK;
    }
  }


  // we nead at least the CRC_A value and all 8 bits of the last byte must be received

  if(*outputBackLen < 2 || _validBits != 0){
    return STATUS_MIFARE_NACK;
  }

  uint8_t controllBuffer[2];

  StatusCode status = 
}


StatusCode RFIDCalculateCRC(uint8_t * inputData, uint8_t inputLenght,uint8_t * outputResult){


  RFIDWriteRegisterTwo(CommandReg,PCD_Idle); // Stop any active comamnd
  RFIDWriteRegisterTwo(DivIrqReg,0x04); // Cleare the crcIrq interrupt request bit
  RFIDWriteRegisterTwo(FIFOLevelReg, 0x80); // FlushBuffer = 1, FIFO initilization
  RFIDWriteRegisterThree(FIFODataReg,inputLenght,inputData); // Write data to the FIFO
  RFIDWriteRegisterTwo(CommandReg,PCD_CalcCRC); // start the calculation


  // Wait for the CRC calculation to complete. Each iteration of the while-loop takes 17.73μs.
  // TODO check/modify for other architectures than Arduino Uno 16bit

  // Wait for the CRC calculation to complete. Each iteration of the while-loop takes 17.73us.
  

  for(uint16_t  i = 5000; i > 0; --i){

    uint8_t temp = RFIDReadRegister(DivIrqReg);

    if(temp & 0x04){
      RFIDWriteRegisterTwo(CommandReg,PCD_Idle); // stop calculation CRC for new content in the FIFO

      //transfer the result from the register to the result buffer

      result[0] = RFIDReadRegister(CRCResultRegL);
      result[1] = RFIDReadRegister(CRCResultRegH);
      return STATUS_OK;
    }
  }

  return STATUS_TIMEOUT;
}

