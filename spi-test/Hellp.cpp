#include "Hellp.h"

Hellp::Hellp(){
	
}

bool  Hellp::try_key(MFRC522::MIFARE_Key *key){
    bool result = false;
    byte buffer[18];
    byte block = 0;
    MFRC522::StatusCode status;
    
    // http://arduino.stackexchange.com/a/14316
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return false;
    if ( ! mfrc522.PICC_ReadCardSerial())
        return false;
    // Serial.println(F("Authenticating using key A..."));
    status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        // Serial.print(F("PCD_Authenticate() failed: "));
        // Serial.println(mfrc522.GetStatusCodeName(status));
        return false;
    }

    // Read block
    byte byteCount = sizeof(buffer);
    status = mfrc522.MIFARE_Read(block, buffer, &byteCount);
    if (status != MFRC522::STATUS_OK) {
        // Serial.print(F("MIFARE_Read() failed: "));
        // Serial.println(mfrc522.GetStatusCodeName(status));
    }
    else {
        // Successful read
        result = true;
        printf("Success with key:");
        dump_byte_array((*key).keyByte, MFRC522::MF_KEY_SIZE);
        printf("\n");
        // Dump block data
        printf("Block "); 
       // printf(block);
        printf(":");
        dump_byte_array(buffer, 16);
        printf("\n");
    }
    printf("\n");

    mfrc522.PICC_HaltA();       // Halt PICC
    mfrc522.PCD_StopCrypto1();  // Stop encryption on PCD
    return result;
}
void Hellp::dump_byte_array(byte *buffer, byte bufferSize) {
  
  for (byte i = 0; i < bufferSize; i++) {
        printf(buffer[i] < 0x10 ? " 0" : " ");
        //printf(buffer[i]);
    }
}