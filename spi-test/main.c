/* The classic "blink" example
 *
 * This sample code is in the public domain.
 */




#include "rfid_rc522.h"

/* This task uses the high level GPIO API (esp_gpio.h) to blink an LED.
 *
 */
void spiTask(void *pvParameters){

uint8_t reg = 0x25;
  while(1){

   RFIDInit();
    printf("%s\n","heelp");
    vTaskDelay(1000 / portTICK_PERIOD_MS);
}

  
}



/* This task demonstrates an alternative way to use raw register
   operations to blink an LED.

   The step that sets the iomux register can't be automatically
   updated from the 'gpio' constant variable, so you need to change
   the line that sets IOMUX_GPIO2 if you change 'gpio'.

   There is no significant performance benefit to this way over the
   blinkenTask version, so it's probably better to use the blinkenTask
   version.

   NOTE: This task isn't enabled by default, see the commented out line in user_init.
*/


void user_init(void)
{
   spi_init(1,SPI_MODE0,SPI_FREQ_DIV_1M, 1, SPI_LITTLE_ENDIAN, true);

    uart_set_baud(0, 115200);
   
    /*const spi_settings_t * settingSpi = {
                                SPI_MODE2,
                                SPI_FREQ_DIV_40M,
                                true,
                                SPI_LITTLE_ENDIAN,
                                true
                              };

    

    bool spiOpenFlag = spi_set_settings(SPI,settingSpi);

    if(!spiOpenFlag){
      printf("Open spi faild.");
    }*/



 
    xTaskCreate(spiTask, "spiTask", 1024, NULL, 2, NULL);
    //xTaskCreate(blinkenRegisterTask, "blinkenRegisterTask", 256, NULL, 2, NULL);
}
