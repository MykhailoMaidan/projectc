#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QTcpServer>
#include <QTcpSocket>
#include <QString>
#include <QByteArray>


class Server: public QObject{

    Q_OBJECT

private:
    QTcpServer* mTcpServer;

    void SendDataToClient(QTcpSocket* clientSocket, QString *str);

public:
    explicit Server(QObject * parent = 0);

public slots:

     void slotNewConnection();
     void slotReadClient();
};

#endif // SERVER_H
