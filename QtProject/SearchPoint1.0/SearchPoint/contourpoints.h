#ifndef CONTOURPOINTS_H
#define CONTOURPOINTS_H

//OpenCv
#include <opencv/cv.h>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include "opencv2/core/utility.hpp"
//#include "opencv2/ximgproc/disparity_filter.hpp"

//c++
#include <vector>
#include <cstring>
#include <iterator>
#include <iostream>

using namespace std;



#define WindowImage "windowImage" // window for the original image
#define WindowGray "windowGray"   // window for the gray image
#define WindowCvCanny "windowCvCanny" // window for the monochrome image

class ContourPoints
{

typedef vector <vector <CvPoint> > VectorPoint;

private:

    char* pImagePath;
    char* pBufNameImage;
    int numberOfContoures;
    CvCapture* pCapturVideo;
    IplImage* pFrameCamera;
    IplImage* pImageOriginal;
    IplImage* pImageGray;
    IplImage* pImageContours;
    IplImage* pPlexusImage;
    CvMemStorage* pStorage;
    CvSeq *pContour;
    CvMat *qMatrix;
    CvMat* pMatrixCameraOne;
    CvMat* pMatrixCameraTwo;
    CvMat* pDistortionCoeffsCameraOne;
    CvMat* pDistortionCoeffsCameraTwo;

    void SaveImageFromCamera(int); // Save image from camera as Enter

public:
    ContourPoints();
    ContourPoints(char*);
    ~ContourPoints();
    void CreateCaptureCameraAndShowStream(); //Stream capture video off web-camera
    void ContouresImage(); // Find contoures image by using cv librarry and IplImage class
    void DrawPointInImage();
    void SmoothingImage();
    void PlexusImage();
    void SetImage(char*);
    void GetContoureMoment(IplImage*,IplImage*);
    void CalibrationCamera();
    void CalcDepthMap(IplImage*, IplImage*);
};

#endif // CONTOURPOINTS_H
