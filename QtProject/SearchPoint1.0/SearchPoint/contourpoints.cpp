#include "contourpoints.h"
// default constructor
ContourPoints::ContourPoints(){

     pImagePath = nullptr;
     pBufNameImage = new char[100];
     numberOfContoures = 0;
     pCapturVideo = nullptr;
     pFrameCamera = nullptr;
     pImageOriginal = nullptr;
     pImageGray = nullptr;
     pImageContours = nullptr;
     pPlexusImage = nullptr;
     pStorage = nullptr;
     pContour = nullptr;
     qMatrix =cvCreateMat(4,4,CV_32F);
     pMatrixCameraOne = cvCreateMat(3,3,CV_32FC1);
     pDistortionCoeffsCameraOne = cvCreateMat(5,1,CV_32FC1);
     //cvCreateData(qMatrix);

     cvmSet(qMatrix,0,0,1.0000);
     cvmSet(qMatrix,0,1,0.00000);
     cvmSet(qMatrix,0,2,0.00000);
     cvmSet(qMatrix,0,3,-327.73883438110352);
     cvmSet(qMatrix,1,0,0.00000);
     cvmSet(qMatrix,1,1,1.00000);
     cvmSet(qMatrix,1,2,0.00000);
     cvmSet(qMatrix,1,3,-239.84486865997314);
     cvmSet(qMatrix,2,0,0.00000);
     cvmSet(qMatrix,2,1,0.00000);
     cvmSet(qMatrix,2,2,0.00000);
     cvmSet(qMatrix,2,3,524.04542174159019);
     cvmSet(qMatrix,3,0,0.00000);
     cvmSet(qMatrix,3,1,0.00000);
     cvmSet(qMatrix,3,2,0.30009508961926923);
     cvmSet(qMatrix,3,3,-1.2438668093184739);






}
// constructor
ContourPoints::ContourPoints(char * imagePath):ContourPoints::ContourPoints(){

    pImagePath = new char[std::strlen(imagePath)+1];
    strcpy(pImagePath,imagePath);
    pImageOriginal = cvLoadImage(pImagePath,1);


}
// destructor
ContourPoints::~ContourPoints(){
    delete pImagePath;
    delete pBufNameImage;
    cvReleaseImage(&pFrameCamera);
    cvReleaseImage(&pImageOriginal);
    cvReleaseImage(&pImageGray);
    cvReleaseImage(&pImageContours);
    cvReleaseMemStorage(&pStorage);
    cvReleaseData(&pContour);
    cvReleaseMat(&qMatrix);
    cvReleaseMat(&pMatrixCameraOne);
    cvReleaseMat(&pMatrixCameraTwo);
    cvReleaseMat(&pDistortionCoeffsCameraOne);
    cvReleaseMat(&pDistortionCoeffsCameraTwo);
    cvReleaseCapture(&pCapturVideo);
    cvDestroyWindow(WindowImage);
    cvDestroyWindow(WindowGray);
    cvDestroyWindow(WindowCvCanny);

}
//function for the save image
void ContourPoints::SaveImageFromCamera(int numberFrame){

            sprintf(pBufNameImage,"Image%d.jpg",numberFrame);// record name file
            cvSaveImage(pBufNameImage,pFrameCamera);// save new file
}
//stream capture from camera
void ContourPoints::CreateCaptureCameraAndShowStream(){

    pCapturVideo = cvCreateCameraCapture(1);

    cvNamedWindow(WindowImage,CV_WINDOW_NORMAL); // Create window for Image

    char keyBreaks;
    int numberFrame = 0;

    do{
        pFrameCamera = cvQueryFrame(pCapturVideo);
        cvShowImage(WindowImage,pFrameCamera);
        cvWaitKey(33);

        if(keyBreaks == 13){
            SaveImageFromCamera(numberFrame);
            ++numberFrame;
        }

    }while(keyBreaks != 27);
}
//finding contoures of the image
void ContourPoints::ContouresImage(){

    pImageGray =cvCreateImage(cvGetSize(pImageOriginal),IPL_DEPTH_8U,1); // create new image
    pImageContours = cvCreateImage(cvGetSize(pImageOriginal),IPL_DEPTH_8U,1);
    pStorage = cvCreateMemStorage(0);

    cvNamedWindow(WindowImage,CV_WINDOW_NORMAL);// create new window for the image
    cvNamedWindow(WindowGray,CV_WINDOW_NORMAL);
    cvNamedWindow(WindowCvCanny,CV_WINDOW_NORMAL);

   cvCvtColor(pImageOriginal,pImageGray,CV_RGB2GRAY);// converting a color image to monochrome image

   cvCanny(pImageGray,pImageContours,100,200,3);
   //cvThreshold(pImageGray,pImageContours,100,200,CV_THRESH_BINARY_INV);
    numberOfContoures = cvFindContours(
                pImageContours
               ,pStorage
               ,&pContour
               ,sizeof(CvContour)
               ,CV_RETR_EXTERNAL
               ,CV_CHAIN_APPROX_NONE
               ,cvPoint(0,0));




    cvShowImage(WindowGray,pImageGray);
    cvShowImage(WindowCvCanny,pImageContours);

    //cvWaitKey(0);// wait in tab
}
//designation of reference points on the image
void ContourPoints::DrawPointInImage(){

    for(CvSeq* seq = pContour; seq != NULL; seq = seq->h_next){
        CvPoint* point = (CvPoint*)cvGetSeqElem(seq,seq->first->count);
        cvCircle(pImageOriginal
                 ,cvPoint(point->x,point->y)
                 ,5
                 ,cvScalarAll(255)
                 );
    }
     cvShowImage(WindowImage,pImageOriginal);// show the window of image
     cvWaitKey(0);
}
//smooth image
void ContourPoints::SmoothingImage(){
    cvSmooth(pImageOriginal,pImageOriginal,CV_MEDIAN,3,3);
}
//convolution image
void ContourPoints::PlexusImage(){
    pImageOriginal = cvLoadImage(pImagePath,1);


    float kernel[9];
    kernel[0] = -0.1;
    kernel[1] = -0.1;
    kernel[2] = -0.1;
    kernel[3] = -0.1;
    kernel[4] = 2;
    kernel[5] = -0.1;
    kernel[6] = -0.1;
    kernel[7] = -0.1;
    kernel[9] = -0.1;

    CvMat kernelMatrix = cvMat(3,3,CV_32FC1,kernel);

    cvFilter2D(pImageOriginal,pImageOriginal,&kernelMatrix, cvPoint(-1,-1));

//   cvNamedWindow(WindowImage,CV_WINDOW_NORMAL);
//   cvShowImage(WindowImage,pImageOriginal);


   cvWaitKey(0);



}
//set new image
void ContourPoints::SetImage(char *imagePath){
    delete pImagePath;
    pImagePath = new char[strlen(imagePath)+1];
    strcpy(pImagePath,imagePath);
}
//points of comparison circuits
void ContourPoints::GetContoureMoment(IplImage* pImageLeft, IplImage* pImageRight){
    assert(pImageLeft != nullptr);
    assert(pImageRight != nullptr);

    IplImage* pImageSrc = cvCloneImage(pImageLeft);
    IplImage* pImageDst = nullptr;
    IplImage* pBinImageLeft = cvCreateImage(cvGetSize(pImageLeft),8,1);
    IplImage* pBinImageRight = cvCreateImage(cvGetSize(pImageRight),8,1);

    IplImage* pRGBImageLeft = cvCreateImage(cvGetSize(pImageLeft),8,3);
    cvConvertImage(pImageLeft,pRGBImageLeft,CV_GRAY2BGR);
    IplImage* pRGBImageRight = cvCreateImage(cvGetSize(pImageRight),8,3);
    cvConvertImage(pImageRight,pRGBImageRight,CV_GRAY2BGR);

    cvCanny(pImageLeft,pBinImageLeft,50,200);
    cvCanny(pImageRight,pBinImageRight,50,200);

    cvNamedWindow(WindowImage,CV_WINDOW_NORMAL);
    cvShowImage(WindowImage,pBinImageLeft);

    cvNamedWindow(WindowGray,CV_WINDOW_NORMAL);


    cvWaitKey(0);

    CvMemStorage* pStorage = cvCreateMemStorage(0);
    CvSeq* pContoureImageLeft = nullptr;
    CvSeq* pContoureImageRight = nullptr;

    int contoursCount = cvFindContours(
                pBinImageLeft
               ,pStorage
               ,&pContoureImageLeft
               ,sizeof(CvContour)
               ,CV_RETR_LIST
               ,CV_CHAIN_APPROX_SIMPLE
               ,cvPoint(0,0)
            );

   CvFont  font;
   cvInitFont(&font,CV_FONT_HERSHEY_PLAIN,1.0,1.0);

   char buf[128];
   int contoure = 0;

   if(pContoureImageLeft != nullptr){
        for(CvSeq* seq = pContoureImageLeft; seq != nullptr; seq = seq->h_next){
            cvDrawContours(
                        pRGBImageLeft
                       ,seq
                       ,CV_RGB(255,0,0)
                       ,CV_RGB(0,0,250)
                       ,0
                       ,1
                       ,8);
        }
        cvShowImage(WindowImage,pRGBImageLeft);
   }

   cvFindContours(
               pBinImageRight
              ,pStorage
              ,&pContoureImageRight
              ,sizeof(CvContour)
              ,CV_RETR_LIST
              ,CV_CHAIN_APPROX_SIMPLE
              ,cvPoint(0,0)
   );



        double differenceMomemt = 1; //difference moment between two image
        int disparity = 0;

        if(pContoureImageLeft != 0 && pContoureImageRight != 0){

            for(CvSeq* seqLeft = pContoureImageLeft; seqLeft != nullptr; seqLeft = seqLeft->h_next ){

                for(CvSeq* seqRight = pContoureImageRight; seqRight != nullptr; seqRight = seqRight->h_next){

                   if(cvMatchShapes(seqLeft,seqRight,CV_CONTOURS_MATCH_I3) < differenceMomemt){

                       cvDrawContours(pRGBImageRight,seqRight,CV_RGB(245,245,0),CV_RGB(25,45,5),0,1,8);
                       disparity = atoi((const char*)seqRight->first->data) - atoi((const char*)seqLeft->first->data);
                   }
                }
            }

            cvNamedWindow(WindowCvCanny,CV_WINDOW_NORMAL);
            cvShowImage(WindowCvCanny,pRGBImageRight);

        }
        cvWaitKey(0);
}
//camera calibration
void ContourPoints::CalibrationCamera(){

    int sizeBoard = 7*7;
    int numberOfImages = 20;
    int cornerCount = 0;
    int frameCount = 0;
    int stepImage  = 0;
    int successImage = 0;
    CvMat* pObjectPoints = cvCreateMat(numberOfImages*sizeBoard,3,CV_32FC1);
    CvMat* pImagePoints = cvCreateMat(numberOfImages*sizeBoard,2,CV_32FC1);
    CvMat* pPointsCount = cvCreateMat(numberOfImages,1,CV_32SC1);
    CvSize boardSize = cvSize(7,7);

    CvCapture* pCaptureCamera = cvCreateCameraCapture(CV_CAP_ANY);
    CvPoint2D32f* cornersPoint = new CvPoint2D32f[sizeBoard];

    IplImage* pFrame   = cvQueryFrame(pCaptureCamera);
    IplImage* pImageGray = cvCreateImage(cvGetSize(pFrame),8,1);

    cvNamedWindow(WindowImage,CV_WINDOW_AUTOSIZE);

    while(successImage < numberOfImages){

        if((frameCount++ % numberOfImages) == 0){
            int foundCorner = cvFindChessboardCorners(
                pFrame
               ,boardSize
               ,cornersPoint
               ,&cornerCount
               ,CV_CALIB_CB_ADAPTIVE_THRESH || CV_CALIB_CB_FILTER_QUADS
                );


    cvCvtColor(pFrame,pImageGray,CV_BGR2GRAY);

    cvFindCornerSubPix(
                pImageGray
               ,cornersPoint
               ,cornerCount
               ,cvSize(11,11)
               ,cvSize(-1,-1)
               ,cvTermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER,30,0.1)
    );

    cvDrawChessboardCorners(
                pFrame
               ,boardSize
               ,cornersPoint
               ,cornerCount
               ,foundCorner
   );


    cvShowImage(WindowImage,pFrame);

    if(cornerCount == sizeBoard){

        stepImage = successImage*sizeBoard;

        for(int i = stepImage, j = 0; j < sizeBoard;++i,++j){
            CV_MAT_ELEM(*pImagePoints,float,i,0) = cornersPoint[j].x;
            CV_MAT_ELEM(*pImagePoints,float,i,1) = cornersPoint[j].y;
            CV_MAT_ELEM(*pObjectPoints,float,i,0) = j / 7;
            CV_MAT_ELEM(*pObjectPoints,float,i,1) = j % 7;
            CV_MAT_ELEM(*pObjectPoints,float,i,0) = 0.0f;
        }

        CV_MAT_ELEM(*pPointsCount,int,successImage,0) = sizeBoard;
        ++successImage;
    }
        }


        int waitKey = cvWaitKey(33);
        if(waitKey == 27){
            break;

        }
        pFrame = cvQueryFrame(pCaptureCamera);
    }

    CvMat* pObjectPoints2 = cvCreateMat(successImage*sizeBoard,3,CV_32FC1);
    CvMat* pImagePoints2 = cvCreateMat(successImage*sizeBoard,2,CV_32FC1);
    CvMat* pPointsCount2 = cvCreateMat(successImage,1,CV_32SC1);

    for(int i = 0; i < successImage*sizeBoard; ++i){
        CV_MAT_ELEM(*pImagePoints2,float,i,0) = CV_MAT_ELEM(*pImagePoints,float,i,0);
        CV_MAT_ELEM(*pImagePoints2,float,i,1) = CV_MAT_ELEM(*pImagePoints,float,i,1);
        CV_MAT_ELEM(*pObjectPoints2,float,i,0) = CV_MAT_ELEM(*pObjectPoints,float,i,0);
        CV_MAT_ELEM(*pObjectPoints2,float,i,1) = CV_MAT_ELEM(*pObjectPoints,float,i,1);
        CV_MAT_ELEM(*pObjectPoints2,float,i,2) = CV_MAT_ELEM(*pObjectPoints,float,i,2);
    }

    for(int i = 0; i < successImage; ++i){
        CV_MAT_ELEM(*pPointsCount2,int,i,0) = CV_MAT_ELEM(*pPointsCount,int,i,0);
    }



    CV_MAT_ELEM(*pMatrixCameraOne,float,0,0) = 1.0f;
    CV_MAT_ELEM(*pMatrixCameraOne,float,1,1) = 1.0f;

    cvCalibrateCamera2(
                pObjectPoints2
               ,pImagePoints2
               ,pPointsCount2
               ,cvGetSize(pFrame)
               ,pMatrixCameraOne
               ,pDistortionCoeffsCameraOne
               ,nullptr
               ,nullptr
               ,0

    );

    cvReleaseMat(&pImagePoints);
    cvReleaseMat(&pObjectPoints);
    cvReleaseMat(&pPointsCount);
}
//depth map
void ContourPoints::CalcDepthMap(IplImage *pImageLeft, IplImage *pImageRight){

    CvSize sizeImage = cvGetSize(pImageLeft);
    IplImage* pImageDispatirity = cvCreateImage(sizeImage,IPL_DEPTH_16S,1);
    CvStereoBMState* pBMSState = cvCreateStereoBMState();
    assert(pBMSState);



    cvFindStereoCorrespondenceBM(pImageLeft,pImageRight,pImageDispatirity,pBMSState);

    pBMSState->SADWindowSize = 0;
    pBMSState->numberOfDisparities = 112;
    pBMSState->preFilterSize = 5;
    pBMSState->preFilterCap = 61;
    pBMSState->textureThreshold = 507;
    pBMSState->uniquenessRatio = 0;
    pBMSState->speckleWindowSize = 0;
    pBMSState->speckleRange =   8;
    pBMSState->disp12MaxDiff = 1;
    pBMSState->minDisparity = -39;


    IplImage* pImageDispatirityVisual = cvCreateImage(sizeImage,IPL_DEPTH_8U,1);
    cvConvertScale(pImageDispatirity,pImageDispatirityVisual,16,0);
    cvNormalize(pImageDispatirity,pImageDispatirityVisual,0,256,CV_MINMAX,CV_8U);
    //IplImage*  p3DImage = cvCreateImage(sizeImage,IPL_DEPTH_32F,3);
    CvMat* p3DImage = cvCreateMat(sizeImage.height,sizeImage.width,CV_32FC3);
    cvReprojectImageTo3D(pImageDispatirity,p3DImage,qMatrix);

    vector<CvPoint3D32f> PointArray;
    CvPoint3D32f imagePoint;

    for(int  i = 0; i < p3DImage->rows; i++){
        float* data = (float*) (p3DImage->data.ptr+i*p3DImage->step);

        for(int j = 0; j < p3DImage->width*3; j = j + 3){
            imagePoint.x = data[i];
            imagePoint.y = data[j+1];
            imagePoint.z = data[j+2];
            PointArray.push_back(imagePoint);
        }
    }

    cvNamedWindow(WindowImage,CV_WINDOW_NORMAL);
    cvNamedWindow(WindowGray,CV_WINDOW_NORMAL);
    cvShowImage(WindowImage,pImageDispatirityVisual);
    cvShowImage(WindowGray,p3DImage);

    cvWaitKey(0);

}






