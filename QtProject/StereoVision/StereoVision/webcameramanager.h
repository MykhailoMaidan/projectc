    #ifndef WEBCAMERAMANAGER_H
    #define WEBCAMERAMANAGER_H

    //Linux
    #include <pthread.h>
    #include <unistd.h>

    //OpenCv
    #include <opencv2/highgui/highgui.hpp>

    //c++
    #include <iostream>
    #include <vector>
    #include <string>


    class WebCameraManager
    {
    public:
        WebCameraManager();
        ~WebCameraManager();
        bool StartParalleCaptureWebCameras(int numberOfWebCameras, std::vector<int>& indexWebCameras);
        bool InitBoardPhotography(int numberOfPhotography);
        bool LoadBoardImage(IplImage *&pLeftImage, IplImage *&pRightImage, int channel, int indexPhoto);

    private:
        const static std::string nameLeftImage;
        const static std::string nameRightImage;
        static std::string sPathToImage;
        pthread_t * thread;

        //func
        static void* Capture(void* arg);
    };
    #endif // WEBCAMERAMANAGER_H
