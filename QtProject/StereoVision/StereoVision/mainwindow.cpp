#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);

    pVision = new StereoVision();
    ui->SADWindowSize_hSlider->setRange(5,255);
    ui->TextureThreshold_hSlider->setRange(0,300);
    ui->UniquenessRatio_hSlider->setRange(0,300);
    ui->MinDisparity_hSlider->setRange(-200,200);
    ui->NumberOfDisparities_hSlider->setRange(0,300);
    ui->SpeckWindowSize->setRange(0,300);
    ui->PreFilterSize_hSlider->setRange(5,255);
    ui->PreFilterCap_hSlider->setRange(1,63);
    ui->Disp12MaxDiff_hSlider->setRange(-200,200);
    ui->SpeckleRange_hSlider->setRange(-200,200);

}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::on_SADWindowSize_hSlider_sliderMoved(int position){
    int temp  = position % 2;

    if(temp == 0){
        position += 1;
    }

    pVision->BMState->SADWindowSize = position;
    ui->SADWindowSize_size_label->setText(QString::number(position));
}

void MainWindow::on_TextureThreshold_hSlider_sliderMoved(int position){
    pVision->BMState->textureThreshold = position;
    ui->TextureThreshold_size_label->setText(QString::number(position));
}

void MainWindow::on_UniquenessRatio_hSlider_sliderMoved(int position){
    pVision->BMState->uniquenessRatio = position;
    ui->UniquenessRatio_size_label->setText(QString::number(position));
}

void MainWindow::on_MinDisparity_hSlider_sliderMoved(int position){
    pVision->BMState->minDisparity = position;
    ui->MinDisparity_size_label->setText(QString::number(position));
}

void MainWindow::on_NumberOfDisparities_hSlider_sliderMoved(int position){

    if((position % 16) == 0 ){

    pVision->BMState->numberOfDisparities = position;

    }else{
        int remainder = position % 16;
        position = position + remainder;
    }
    ui->NumberOfDisparities_size_label->setText(QString::number(position));
}

void MainWindow::on_SpeckleRange_hSlider_sliderMoved(int position){

    pVision->BMState->speckleRange = position;
    ui->SpeckleRange_size_label->setText(QString::number(position));
}

void MainWindow::on_PreFilterSize_hSlider_sliderMoved(int position){
    int temp = position % 2;
    if(temp == 0){
       position += 1;
    }
    pVision->BMState->preFilterSize = position;
    ui->PreFilterSize_size_label->setText(QString::number(position));
}

void MainWindow::on_PreFilterCap_hSlider_sliderMoved(int position){
    int temp = position % 2;
    if(temp == 0){
        position += 1;
    }
    pVision->BMState->preFilterCap = position;
    ui->PreFilterCap_size_label->setText(QString::number(position));
}

void MainWindow::on_Disp12MaxDiff_hSlider_sliderMoved(int position){
    pVision->BMState->disp12MaxDiff = position;
    ui->Disp12MaxDiff_size_label->setText(QString::number(position));
}

void MainWindow::on_SpeckWindowSize_sliderMoved(int position){
    pVision->BMState->speckleWindowSize = position;
    ui->SpeckWindowSize_size_label->setText(QString::number(position));
}

void MainWindow::on_Start_pushButton_clicked(){
    pVision->StereoVisionStart();

}
