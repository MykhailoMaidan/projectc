#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "stereovision.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_SADWindowSize_hSlider_sliderMoved(int position);

    void on_TextureThreshold_hSlider_sliderMoved(int position);

    void on_UniquenessRatio_hSlider_sliderPressed();

    void on_UniquenessRatio_hSlider_sliderMoved(int position);

    void on_MinDisparity_hSlider_sliderMoved(int position);

    void on_NumberOfDisparities_hSlider_sliderMoved(int position);

    void on_SpeckleRange_hSlider_sliderMoved(int position);

    void on_PreFilterSize_hSlider_sliderMoved(int position);

    void on_PreFilterCap_hSlider_sliderMoved(int position);

    void on_Disp12MaxDiff_hSlider_sliderMoved(int position);

    void on_SpeckWindowSize_sliderMoved(int position);

    void on_Start_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    StereoVision* pVision;
};

#endif // MAINWINDOW_H
