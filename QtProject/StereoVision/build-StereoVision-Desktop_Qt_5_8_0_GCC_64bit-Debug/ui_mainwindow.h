/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *Start_pushButton;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *SADWindowSize_label;
    QLabel *SADWindowSize_size_label;
    QSlider *SADWindowSize_hSlider;
    QLabel *TextureThreshold_label;
    QLabel *TextureThreshold_size_label;
    QSlider *TextureThreshold_hSlider;
    QLabel *UniquenessRatio_label;
    QSlider *UniquenessRatio_hSlider;
    QLabel *MinDisparity_label;
    QLabel *MinDisparity_size_label;
    QSlider *MinDisparity_hSlider;
    QLabel *NumberOfDisparities_label;
    QLabel *NumberOfDisparities_size_label;
    QSlider *NumberOfDisparities_hSlider;
    QLabel *SpeckleRange_label;
    QLabel *SpeckleRange_size_label;
    QSlider *SpeckleRange_hSlider;
    QLabel *PreFilterSize_label;
    QLabel *PreFilterSize_size_label;
    QSlider *PreFilterSize_hSlider;
    QLabel *PreFilterCap_label;
    QLabel *PreFilterCap_size_label;
    QSlider *PreFilterCap_hSlider;
    QLabel *Disp12MaxDiff_label;
    QLabel *Disp12MaxDiff_size_label;
    QSlider *Disp12MaxDiff_hSlider;
    QLabel *SpeckWindowSize_label;
    QLabel *SpeckWindowSize_size_label;
    QSlider *SpeckWindowSize;
    QLabel *UniquenessRatio_size_label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(300, 540);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Start_pushButton = new QPushButton(centralWidget);
        Start_pushButton->setObjectName(QStringLiteral("Start_pushButton"));
        Start_pushButton->setGeometry(QRect(10, 10, 89, 25));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(12, 41, 252, 436));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        SADWindowSize_label = new QLabel(layoutWidget);
        SADWindowSize_label->setObjectName(QStringLiteral("SADWindowSize_label"));

        gridLayout->addWidget(SADWindowSize_label, 0, 0, 1, 3);

        SADWindowSize_size_label = new QLabel(layoutWidget);
        SADWindowSize_size_label->setObjectName(QStringLiteral("SADWindowSize_size_label"));

        gridLayout->addWidget(SADWindowSize_size_label, 0, 3, 1, 4);

        SADWindowSize_hSlider = new QSlider(layoutWidget);
        SADWindowSize_hSlider->setObjectName(QStringLiteral("SADWindowSize_hSlider"));
        SADWindowSize_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(SADWindowSize_hSlider, 1, 0, 1, 1);

        TextureThreshold_label = new QLabel(layoutWidget);
        TextureThreshold_label->setObjectName(QStringLiteral("TextureThreshold_label"));

        gridLayout->addWidget(TextureThreshold_label, 2, 0, 1, 4);

        TextureThreshold_size_label = new QLabel(layoutWidget);
        TextureThreshold_size_label->setObjectName(QStringLiteral("TextureThreshold_size_label"));

        gridLayout->addWidget(TextureThreshold_size_label, 2, 5, 1, 2);

        TextureThreshold_hSlider = new QSlider(layoutWidget);
        TextureThreshold_hSlider->setObjectName(QStringLiteral("TextureThreshold_hSlider"));
        TextureThreshold_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(TextureThreshold_hSlider, 3, 0, 1, 1);

        UniquenessRatio_label = new QLabel(layoutWidget);
        UniquenessRatio_label->setObjectName(QStringLiteral("UniquenessRatio_label"));

        gridLayout->addWidget(UniquenessRatio_label, 4, 0, 1, 3);

        UniquenessRatio_hSlider = new QSlider(layoutWidget);
        UniquenessRatio_hSlider->setObjectName(QStringLiteral("UniquenessRatio_hSlider"));
        UniquenessRatio_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(UniquenessRatio_hSlider, 5, 0, 1, 1);

        MinDisparity_label = new QLabel(layoutWidget);
        MinDisparity_label->setObjectName(QStringLiteral("MinDisparity_label"));

        gridLayout->addWidget(MinDisparity_label, 6, 0, 1, 1);

        MinDisparity_size_label = new QLabel(layoutWidget);
        MinDisparity_size_label->setObjectName(QStringLiteral("MinDisparity_size_label"));

        gridLayout->addWidget(MinDisparity_size_label, 6, 1, 1, 6);

        MinDisparity_hSlider = new QSlider(layoutWidget);
        MinDisparity_hSlider->setObjectName(QStringLiteral("MinDisparity_hSlider"));
        MinDisparity_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(MinDisparity_hSlider, 7, 0, 1, 1);

        NumberOfDisparities_label = new QLabel(layoutWidget);
        NumberOfDisparities_label->setObjectName(QStringLiteral("NumberOfDisparities_label"));

        gridLayout->addWidget(NumberOfDisparities_label, 8, 0, 1, 6);

        NumberOfDisparities_size_label = new QLabel(layoutWidget);
        NumberOfDisparities_size_label->setObjectName(QStringLiteral("NumberOfDisparities_size_label"));

        gridLayout->addWidget(NumberOfDisparities_size_label, 8, 6, 1, 1);

        NumberOfDisparities_hSlider = new QSlider(layoutWidget);
        NumberOfDisparities_hSlider->setObjectName(QStringLiteral("NumberOfDisparities_hSlider"));
        NumberOfDisparities_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(NumberOfDisparities_hSlider, 9, 0, 1, 1);

        SpeckleRange_label = new QLabel(layoutWidget);
        SpeckleRange_label->setObjectName(QStringLiteral("SpeckleRange_label"));

        gridLayout->addWidget(SpeckleRange_label, 10, 0, 1, 2);

        SpeckleRange_size_label = new QLabel(layoutWidget);
        SpeckleRange_size_label->setObjectName(QStringLiteral("SpeckleRange_size_label"));

        gridLayout->addWidget(SpeckleRange_size_label, 10, 2, 1, 5);

        SpeckleRange_hSlider = new QSlider(layoutWidget);
        SpeckleRange_hSlider->setObjectName(QStringLiteral("SpeckleRange_hSlider"));
        SpeckleRange_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(SpeckleRange_hSlider, 11, 0, 1, 1);

        PreFilterSize_label = new QLabel(layoutWidget);
        PreFilterSize_label->setObjectName(QStringLiteral("PreFilterSize_label"));

        gridLayout->addWidget(PreFilterSize_label, 12, 0, 1, 1);

        PreFilterSize_size_label = new QLabel(layoutWidget);
        PreFilterSize_size_label->setObjectName(QStringLiteral("PreFilterSize_size_label"));

        gridLayout->addWidget(PreFilterSize_size_label, 12, 1, 1, 6);

        PreFilterSize_hSlider = new QSlider(layoutWidget);
        PreFilterSize_hSlider->setObjectName(QStringLiteral("PreFilterSize_hSlider"));
        PreFilterSize_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(PreFilterSize_hSlider, 13, 0, 1, 1);

        PreFilterCap_label = new QLabel(layoutWidget);
        PreFilterCap_label->setObjectName(QStringLiteral("PreFilterCap_label"));

        gridLayout->addWidget(PreFilterCap_label, 14, 0, 1, 1);

        PreFilterCap_size_label = new QLabel(layoutWidget);
        PreFilterCap_size_label->setObjectName(QStringLiteral("PreFilterCap_size_label"));

        gridLayout->addWidget(PreFilterCap_size_label, 14, 1, 2, 6);

        PreFilterCap_hSlider = new QSlider(layoutWidget);
        PreFilterCap_hSlider->setObjectName(QStringLiteral("PreFilterCap_hSlider"));
        PreFilterCap_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(PreFilterCap_hSlider, 15, 0, 1, 1);

        Disp12MaxDiff_label = new QLabel(layoutWidget);
        Disp12MaxDiff_label->setObjectName(QStringLiteral("Disp12MaxDiff_label"));

        gridLayout->addWidget(Disp12MaxDiff_label, 16, 0, 1, 2);

        Disp12MaxDiff_size_label = new QLabel(layoutWidget);
        Disp12MaxDiff_size_label->setObjectName(QStringLiteral("Disp12MaxDiff_size_label"));

        gridLayout->addWidget(Disp12MaxDiff_size_label, 16, 2, 1, 5);

        Disp12MaxDiff_hSlider = new QSlider(layoutWidget);
        Disp12MaxDiff_hSlider->setObjectName(QStringLiteral("Disp12MaxDiff_hSlider"));
        Disp12MaxDiff_hSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Disp12MaxDiff_hSlider, 17, 0, 1, 1);

        SpeckWindowSize_label = new QLabel(layoutWidget);
        SpeckWindowSize_label->setObjectName(QStringLiteral("SpeckWindowSize_label"));

        gridLayout->addWidget(SpeckWindowSize_label, 18, 0, 1, 5);

        SpeckWindowSize_size_label = new QLabel(layoutWidget);
        SpeckWindowSize_size_label->setObjectName(QStringLiteral("SpeckWindowSize_size_label"));

        gridLayout->addWidget(SpeckWindowSize_size_label, 18, 5, 1, 2);

        SpeckWindowSize = new QSlider(layoutWidget);
        SpeckWindowSize->setObjectName(QStringLiteral("SpeckWindowSize"));
        SpeckWindowSize->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(SpeckWindowSize, 19, 0, 1, 1);

        UniquenessRatio_size_label = new QLabel(layoutWidget);
        UniquenessRatio_size_label->setObjectName(QStringLiteral("UniquenessRatio_size_label"));

        gridLayout->addWidget(UniquenessRatio_size_label, 4, 4, 1, 3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 300, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        Start_pushButton->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
        SADWindowSize_label->setText(QApplication::translate("MainWindow", "SADWindowSize", Q_NULLPTR));
        SADWindowSize_size_label->setText(QString());
        TextureThreshold_label->setText(QApplication::translate("MainWindow", "TextureThreshold", Q_NULLPTR));
        TextureThreshold_size_label->setText(QString());
        UniquenessRatio_label->setText(QApplication::translate("MainWindow", "UniquenessRatio", Q_NULLPTR));
        MinDisparity_label->setText(QApplication::translate("MainWindow", "MinDisparity", Q_NULLPTR));
        MinDisparity_size_label->setText(QString());
        NumberOfDisparities_label->setText(QApplication::translate("MainWindow", "NumberOfDisparities", Q_NULLPTR));
        NumberOfDisparities_size_label->setText(QString());
        SpeckleRange_label->setText(QApplication::translate("MainWindow", "SpeckleRange", Q_NULLPTR));
        SpeckleRange_size_label->setText(QString());
        PreFilterSize_label->setText(QApplication::translate("MainWindow", "PreFilterSize", Q_NULLPTR));
        PreFilterSize_size_label->setText(QString());
        PreFilterCap_label->setText(QApplication::translate("MainWindow", "PreFilterCap", Q_NULLPTR));
        PreFilterCap_size_label->setText(QString());
        Disp12MaxDiff_label->setText(QApplication::translate("MainWindow", "Disp12MaxDiff", Q_NULLPTR));
        Disp12MaxDiff_size_label->setText(QString());
        SpeckWindowSize_label->setText(QApplication::translate("MainWindow", "SpeckWindowSize", Q_NULLPTR));
        SpeckWindowSize_size_label->setText(QString());
        UniquenessRatio_size_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
